import {Injectable} from "angular2/core"
import {Storage} from "./storage"

@Injectable()
export class UserService{
	private user : any = undefined;
	private key : string = "user";
	private storage : Storage = undefined;
	constructor(storage :Storage){ 
		this.storage = storage;
	}

	public get(){
		this.user = this.storage.get(this.key);
		return this.user;
	}
	public set(user){
		this.user = user;
		this.storage.set(this.key, this.user);
	}
}
