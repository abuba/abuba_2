import {Injectable} from "angular2/core"
import {Storage} from "../storage"
import {Ajax} from "../ajax"
import {UserService} from "../user"

import {Observable} from 'rxjs/Observable';

@Injectable()
export class PreschoolService{
	private preschoolList : any = undefined;
	private key : string = "parent.preschool";
	private storage : Storage = undefined;
	private ajax : Ajax = undefined;
	private userService : UserService = undefined;

	public static willUpdate : boolean = true;

	constructor(storage :Storage, ajax: Ajax, userService : UserService){ 
		this.storage = storage;
		this.ajax = ajax;
		this.userService = userService;
	}

	public get(){
		this.preschoolList = this.storage.get(this.key);
		if(this.preschoolList && PreschoolService.willUpdate == false){
			return new Observable(observer => {
				observer.next(this.preschoolList);
				observer.complete();
			});
		}else{
			return this.ajax.get({
				url : "/parents/"+this.userService.get().id+"/preschools"
			}).map(
				data => {
					if(data.result == "success"){
						PreschoolService.willUpdate = false;
						this.set(data.data);
						return data.data;
					}else{
						return undefined;
					}
				},
				err => {
					console.log(err);
				}
			);

		}
	}
	public set(preschoolList){
		this.preschoolList = preschoolList;
		this.storage.set(this.key, this.preschoolList);
	}
}
