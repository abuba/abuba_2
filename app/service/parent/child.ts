import {Injectable, Component} from "angular2/core"
import {Storage} from "../storage"
import {Ajax} from "../ajax"
import {UserService} from "../user"
import {PreschoolService} from "./preschool"

import {Observable} from 'rxjs/Observable';


@Injectable()
export class ChildService{
	private childList : any = undefined;
	private key : string = "parent.child";
	private storage : Storage = undefined;
	private ajax : Ajax = undefined;
	private userService : UserService = undefined;
	public static willUpdate : boolean = true;
	constructor(storage :Storage, ajax: Ajax, userService : UserService){ 
		this.storage = storage;
		this.ajax = ajax;
		this.userService = userService;
		console.log("ChildService constructor");
	}

	public get(){
		this.childList = this.storage.get(this.key);
		if(this.childList && ChildService.willUpdate == false){
			return new Observable(observer => {
				observer.next(this.childList);
				observer.complete();
			});
		}else{
			return this.ajax.get({
				url : "/parents/"+this.userService.get().id+"/children"
			}).map(
				data => {
					if(data.result == "success"){
						this.set(data.data);
						ChildService.willUpdate = false;
						return data.data;
					}else {
						return undefined;
					}
				},
				err => {
					console.log(err);
				}
			);

		}
	}
	public add(child){
		this.childList = this.storage.get(this.key);
		return this.ajax.post({
			url:"/children/"+this.userService.get().id,
			data:child
		}).map(
			data => {
				if(data.result == "success"){
					this.childList.push(data.data);
					this.set(this.childList);
					PreschoolService.willUpdate = true;
					return data.data;
				}else  {
					return undefined;
				}
			},
			err => console.log(err)
		);
	}
	public set(childList){
		this.childList = childList;
		this.storage.set(this.key, this.childList);
	}
}
