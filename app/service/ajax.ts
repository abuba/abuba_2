import {Http, Headers, Response} from "angular2/http"
import {Injectable} from "angular2/core"

import 'rxjs/Rx';

@Injectable()
export class Ajax{
	private hostURL : string = "http://scccc.vps.phps.kr:8000";
	constructor(private http :Http){ }


	get (arg){
		return this.http.get(this.hostURL+arg.url)
			.map( res => {
				return res.json()
			});
	}
	post (arg){
		let headers = new Headers({"Content-Type" : "application/json"});
		return this.http.post(this.hostURL+arg.url, JSON.stringify(arg.data), {headers: headers})
			.map(res => {
				return res.json();
			});
	}
	put (arg){
		let headers = new Headers({"Content-Type" : "application/json"});
		return this.http.put(this.hostURL+arg.url, JSON.stringify(arg.data), {headers: headers})
			.map(res => res.json());
	}
	delete (arg){
		return this.http.delete(this.hostURL+arg.url).map(res => res.json());
	}
}
