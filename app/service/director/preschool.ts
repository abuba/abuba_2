import {Injectable, Component} from "angular2/core"
import {Storage} from "../storage"
import {Ajax} from "../ajax"
import {UserService} from "../user"
import {Observable} from 'rxjs/Observable';


@Injectable()
export class PreschoolService{
	private preschoolList : any = undefined;
	private key : string = "director.preschool";
	public static willUpdate : boolean = true;

	constructor(
		private storage :Storage, 
		private ajax: Ajax, 
		private userService : UserService){ 
	}

	public get(){
		this.preschoolList = this.storage.get(this.key);
		if(this.preschoolList && PreschoolService.willUpdate == false){
			return new Observable(observer => {
				observer.next(this.preschoolList);
				observer.complete();
			});
		}else{
			return this.ajax.get({
				url : "/directors/"+this.userService.get().id+"/preschools"
			}).map(
				data => {
					if(data.result == "success"){
						this.set(data.data);
						return data.data;
					}else {
						return undefined;
					}
				},
				err => {
					console.log(err);
				}
			);

		}
	}
	public add(child){
		this.preschoolList = this.storage.get(this.key);
		return this.ajax.post({
			url:"/children/"+this.userService.get().id,
			data:child
		}).map(
			data => {
				if(data.result == "success"){
					this.preschoolList.push(data.data);
					this.set(this.preschoolList);
					PreschoolService.willUpdate = true;
					return data.data;
				}else  {
					return undefined;
				}
			},
			err => console.log(err)
		);
	}
	public set(preschoolList){
		this.preschoolList = preschoolList;
		this.storage.set(this.key, this.preschoolList);
	}
}
