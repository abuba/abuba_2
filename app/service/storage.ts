import {Injectable} from "angular2/core"

@Injectable()
export class Storage{
	constructor(){ }


	set(key, value){
		window.localStorage.setItem(key, JSON.stringify(value));
	}
	get(key){
		return JSON.parse(window.localStorage.getItem(key));
	}
}
