﻿import {Page, NavController, IonicApp} from 'ionic-angular';
import {LoginPage} from "../../../login/login";


@Page({
  templateUrl: 'build/pages/parent/home/pref/pref.html'
})
export class PrefPage {
	constructor(
		private nav:NavController, 
		private app : IonicApp
		   ){
	}
	logout (){
		window.localStorage.clear();
	//	this.nav.setRoot(LoginPage);
		let nav = this.app.getComponent("rootNavController");
		nav.setRoot(LoginPage);
		
	}
}
