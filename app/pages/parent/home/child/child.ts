﻿import {Page, NavController, IonicApp} from 'ionic-angular';

import {ChildService} from "../../../../service/parent/child";
import {Ajax} from "../../../../service/ajax";
import {AddChildPage} from "../../addChild/addChild";




@Page({
  templateUrl: 'build/pages/parent/home/child/child.html',
  providers: [ChildService, Ajax]
})
export class ChildPage {
	childList : any = undefined;
	showSpinner : boolean = false;
	constructor(
		private childService : ChildService, 
		private nav: NavController,
		private app : IonicApp
	){

	}
	onPageDidEnter(){
		this.showSpinner = true;
		this.childService.get().subscribe(
			data => {
				this.showSpinner = false;
				this.childList = data
			}
		);
	}
	addChild(){
	       	//this.nav.push(AddChildPage);;
		let nav = this.app.getComponent("rootNavController");
	       	nav.push(AddChildPage);;
	}
}
