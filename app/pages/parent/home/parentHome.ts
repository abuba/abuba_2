﻿import {Page} from 'ionic-angular';
import {ChildPage} from './child/child';
import {PreschoolPage} from './preschool/preschool';
import {PrefPage} from './pref/pref';


@Page({
  templateUrl: 'build/pages/parent/home/tabs.html'
})
export class ParentHomePage{
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = ChildPage;
  tab2Root: any = PreschoolPage;
  tab3Root: any = PrefPage;
}
