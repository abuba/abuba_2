﻿import {Page, NavController, ActionSheet, IonicApp } from 'ionic-angular';
import {ViewChild} from 'angular2/core';

import {PreschoolService} from "../../../../service/parent/preschool";
import {Ajax} from "../../../../service/ajax";

import {TestPage} from "../../../test/test";

@Page({
  templateUrl: 'build/pages/parent/home/preschool/preschool.html',
  providers:[PreschoolService, Ajax]
})
export class PreschoolPage{
	preschoolList : any = undefined;
	moreMenu : ActionSheet = undefined;
	showSpinner : boolean = false;
	@ViewChild("rootNavController") nav2 : NavController;
	constructor(
		private preschoolService : PreschoolService,
		private nav : NavController,
		private app : IonicApp
	){
	}

	onPageDidEnter(){
		this.showSpinner = true;	
		this.preschoolService.get()
		.subscribe(
			data =>{
				this.showSpinner = false;	
				this.preschoolList = data;
			}
		);
	}
	test(){
		//this.nav.push(TestPage);
		//
		//

		let nav = this.app.getComponent("rootNavController");
		nav.push(TestPage);
		//this.nav2.push(TestPage);

	}
	showMore(preschool){
		this.moreMenu = ActionSheet.create({
			title : preschool.preschool_name,
			buttons:[
				{
					text: "유치원 정보",
					icon : "home",
					handler : () => {
					}
				},
				{
					text: "닫기",
					icon : "close",
					role : "cancel" 
				},
				{
					text: "일정표",
					icon : "calendar",
					handler : () => {
					}
				},
			]

		});
		this.nav.present(this.moreMenu);

	}

}
