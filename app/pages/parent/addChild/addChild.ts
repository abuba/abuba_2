﻿import {Page, NavController, Modal} from 'ionic-angular';
import {NgForm} from 'angular2/common';

import {ChildService} from "../../../service/parent/child";
import {Ajax} from "../../../service/ajax";
import {UserService} from "../../../service/user";

import {SearchPreschoolModal} from "../../common/modal/searchPreschool/searchPreschool" 


@Page({
  templateUrl: 'build/pages/parent/addChild/addChild.html',
  providers: [ChildService, Ajax, UserService]
})
export class AddChildPage {

	newChild : any = {
		photo : "img/user_placeholder.png"
	};
	classList : any = [];
	constructor(
		private childService : ChildService,
		private ajax : Ajax,
		private user : UserService,
		private nav : NavController){
	}


	searchPreschool(){
		let searchPreschoolModal = Modal.create(SearchPreschoolModal);
		searchPreschoolModal.onDismiss(preschool => {
			if(preschool){
				this.newChild.preschool_name = preschool.preschool_name;
				this.newChild.preschool_id = preschool.preschool_id;
				this.getPreschoolClass();
			}
		});
		this.nav.present(searchPreschoolModal);

	}
	getPreschoolClass(){
		this.ajax.get({
			url : "/preschools/"+this.newChild.preschool_id+"/classes"
		}).subscribe(data => {
			this.classList = data.data;
			console.log(this.classList);
		});
	}
	addChild(){
		this.newChild.mem_id = this.user.get().id;
		this.childService.add(this.newChild)
		.subscribe(
			data => {
				this.nav.pop();
			},
			err => console.log(err)
		);
	}
}
