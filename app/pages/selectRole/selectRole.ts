﻿import {Page, NavController, ViewController} from 'ionic-angular';
import {UserService} from "../../service/user";
import {Ajax} from "../../service/ajax";

import {ParentHomePage} from "../parent/home/parentHome";
import {TeacherHomePage} from "../teacher/home/home";

@Page({
  templateUrl: 'build/pages/selectRole/selectRole.html',
  providers:[UserService, Ajax]
})
export class SelectRolePage {
	nav : NavController = undefined;
	viewCtrl : ViewController = undefined;
	user : any = undefined;
	ajax : Ajax = undefined;
	userService : UserService =undefined;
	constructor(nav : NavController, viewCtrl : ViewController, userService : UserService, ajax : Ajax){
		this.nav  = nav;
		this.viewCtrl = viewCtrl;
		this.user = userService.get();
		this.userService = userService;
		this.ajax = ajax;
	}

	/*
	onPageWillEnter(){
		this.nav.remove(0, this.nav.length()-1);
		this.viewCtrl.showBackButton(false);
	}
	onPageDidEnter(){

	}
	*/


	setRole(role){
		this.ajax.put({
			url : "/users/"+this.user.id+"/type",
			data : {
				mem_type : role
			}
		}).subscribe(
			res => {
				this.userService.set(res.data);
				if(role == 1){
					this.nav.setRoot(ParentHomePage);
				}else if(role == 2){
					this.nav.setRoot(TeacherHomePage);
				}
			}, 
			err => {
				console.log(err);
			}
		);
	}


}
