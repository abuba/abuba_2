﻿import {Page, NavController} from 'ionic-angular';


@Page({
  templateUrl: 'build/pages/test/test.html'
})
export class TestPage {
	nav:NavController = undefined;
	constructor(nav:NavController){
		this.nav = nav;
	}
}
