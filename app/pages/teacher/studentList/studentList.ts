﻿import {Page, NavController, ActionSheet} from 'ionic-angular';
import {NgForm} from 'angular2/common';

import {Ajax} from "../../../service/ajax";
import {UserService} from "../../../service/user";

import {SearchFilter} from "../../../filter/search";


@Page({
  templateUrl: 'build/pages/teacher/studentList/studentList.html',
  providers: [Ajax, UserService],
  pipes:[SearchFilter]
})
export class StudentListPage {

	private approvedList :any = [];
	private notApprovedList :any = [];
	private keyword : string = "";
	constructor(
		private ajax : Ajax,
		private user : UserService,
		private nav : NavController){
	
	}
	onPageWillEnter(){
		this.ajax.get({
			url:"/teachers/"+this.user.get().id+"/students"
		}).subscribe(
			data => {
				if(data.result == "success"){
					this.approvedList= data.data.approvedList;
					this.notApprovedList= data.data.notApprovedList;
				}
			},
			err => console.log(err)
		);
	}
	showApprovePopup(student){
		let actionSheet = ActionSheet.create({
			title:student.name +"("+student.class_name+")"	,
			buttons:[
				{
					text : "승인하기",
					icon : "checkmark",
					handler: () => {
					}
				},
				{
					text : "승인거부",
					icon : "hand",
					handler: () => {
					}
				},

			]
		});
		this.nav.present(actionSheet);
	}
	goStudent(student){

	}

}


