﻿import {Page, NavController, ViewController, NavParams, ActionSheet} from 'ionic-angular';
import {UserService} from "../../../service/user";


@Page({
  templateUrl: 'build/pages/teacher/home/main.html',
  providers: [UserService]
})
export class TeacherMainPage{
	private preschool : any = undefined;
	constructor(
		private user : UserService,
		private nav : NavController,
		private viewCtrl : ViewController,
		private navParams : NavParams
		){


	}
	onPageWillEnter(){
		
	}
	onPageDidEnter(){
	}
	showMenu(){
		let actionSheet = ActionSheet.create({
			buttons: [
				{
					text: "공지 작성",
					icon : "megaphone",
					handler: () => {
					}
				},{
					text: "알림장 작성",
					icon: "create",
					handler: () => {


					}
				},{
					text: "일정 추가",
					icon : "calendar",
					handler: () => {
					}
				},{
					text: '취소',
					icon : "close",
					role: 'cancel',
					handler: () => {
						console.log('Cancel clicked');
					}
				}
			]
		});
		this.nav.present(actionSheet);
	}

}
