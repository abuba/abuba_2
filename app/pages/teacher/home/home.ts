﻿import {Page, NavController, ViewController, IonicApp} from 'ionic-angular';
import {UserService} from "../../../service/user";
import {Ajax} from "../../../service/ajax";
import {TeacherMainPage} from "./main";

import {StudentListPage} from "../studentList/studentList"

import {TestPage} from "../../test/test";
import {LoginPage} from "../../login/login";


@Page({
  templateUrl: 'build/pages/teacher/home/home.html',
  providers: [Ajax, UserService]
})
export class TeacherHomePage{
	private preschoolList :any= [];
	private rootPage : any = undefined;
	private rootNav : any = undefined;
	constructor(
		private user : UserService,
		private nav : NavController,
		private viewCtrl : ViewController,
		private app : IonicApp
	){
		this.rootPage = TeacherMainPage;

		this.rootNav = this.app.getComponent("rootNavController");

	}
	onPageWillEnter(){
	}
	onPageDidEnter(){
	}

	student(){
		this.rootNav.push(StudentListPage);



	}
	preschool(){
		this.rootNav.push(StudentListPage);

	}

	logout(){
		window.localStorage.clear();
		this.rootNav.setRoot(LoginPage);
	}


}
