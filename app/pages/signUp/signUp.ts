﻿import {Page, NavController} from 'ionic-angular';
import {SelectRolePage} from "../selectRole/selectRole"
import {NgForm} from "angular2/common" 
import {Ajax} from "../../service/ajax"
import {UserService} from "../../service/user"



@Page({
  templateUrl: 'build/pages/signUp/signUp.html',
  providers:[Ajax]
})


export class SignUpPage {

	newUser : any = {};
	password_c : string = undefined;
	errMsg : any = {}
	isValid : boolean = true;

	constructor(private nav:NavController,
		private ajax : Ajax,
		private userService : UserService
		   ){ }
	goSignUp(){
		this.isValid = true;
		if(this.password_c && this.newUser.password && this.password_c != this.newUser.password){
			this.isValid = this.isValid && false;
			this.errMsg.password_c= "비밀번호가 일치하지 않습니다.";
		}

		if(this.isValid){
			this.ajax.post({
				url:"/users",
				data:this.newUser
			}).subscribe(
				data => {
					if(data.result == "success"){
						this.userService.set(data.data);
						this.nav.setRoot(SelectRolePage);
					}
				},
				err=> console.log(err)
			);

		}
	}
	chkId(){
		this.ajax.get({
			url :"/users/exist/"+this.newUser.user_id
		}).subscribe(
			data => {
				if(data.result == "success"){
					if(data.exist){
						this.errMsg.user_id ="이미 가입된 아이디입니다.";
						this.isValid = false;
					}
				}
			},
			err => console.log(err)
		);
		
	}
}

