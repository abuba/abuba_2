﻿import {Page, NavController} from 'ionic-angular';
import {SignUpPage} from "../signUp/signUp"



@Page({
  templateUrl: 'build/pages/terms/terms.html',
})


export class TermsPage {


	nav : NavController = undefined;
	constructor(nav:NavController){
		this.nav = nav;
	}
	goSignUp(){
		this.nav.push(SignUpPage);
	}
}

