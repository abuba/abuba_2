﻿import {Page, NavController} from 'ionic-angular';
/*
import {Ajax} from "service/ajax"
import {UserService} from "service/user"
import {ParentHomePage} from "pages/parent/home/parentHome"
import {SelectRolePage} from "pages/selectRole/selectRole"
import {TermsPage} from "pages/terms/terms"
*/
import {Ajax} from "../../service/ajax"
import {UserService} from "../../service/user"
import {SelectRolePage} from "../selectRole/selectRole"
import {ParentHomePage} from "../parent/home/parentHome"
import {DirectorHomePage} from "../director/home/home"
import {TeacherHomePage} from "../teacher/home/home"
import {TermsPage} from "../terms/terms"


import {ChildService} from "../../service/parent/child"

@Page({
  templateUrl: 'build/pages/login/login.html',
  providers:[Ajax, UserService, ChildService]
})


export class LoginPage {

	loginInfo :any = {
		user_id : undefined,
		password: undefined,
	};
	nowLogining : boolean = false;
	
	ajax : Ajax = undefined;
	nav : NavController = undefined;
	userService : UserService = undefined;
	loginError : string = undefined;
	childService : ChildService = undefined;

	//termsPage : TermsPage  =  TermsPage;

	constructor( ajax: Ajax, nav : NavController, userService : UserService, childService : ChildService){
		this.ajax = ajax;
		this.nav = nav;
		this.userService = userService;
		this.childService = childService;
	}
	login(){
		this.nowLogining = true;
		this.ajax.post({
			url:"/login",
			data : this.loginInfo
		}) 
		.subscribe(
			loginResult => {
				this.nowLogining = false;
				if(loginResult.result == "success"){
					this.navigate(loginResult)
				}else if(loginResult.result == "fail"){
					if(loginResult.error.code == 401){
						this.loginError = "로그인 정보가 없습니다.";

					}
					console.log(loginResult);
				}
			},
			err  => {
				this.nowLogining = false;
				console.log(err)
			}
		);
	}
	ongPageDidEnter(){
		this.nowLogining = false;
		this.nav.remove(0, this.nav.length()-1);
	}
	goTerms(){
		this.nav.push(TermsPage);
	}

	navigate(user){
		user = user.data;
		this.userService.set(user);
		console.log(user);
		if(user.mem_type == 0){
			this.nav.setRoot(SelectRolePage);
		}else if(user.mem_type == 1){
			this.nav.setRoot(ParentHomePage);
		}else if(user.mem_type == 2){
			this.nav.setRoot(TeacherHomePage);
		}else if(user.mem_type == 3){
			this.nav.setRoot(DirectorHomePage);
		}else if(user.mem_type == 4){
			//this.nav.setRoot(DirectorHomePage);
		}
	}



}

