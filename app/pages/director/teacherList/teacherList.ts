﻿import {Page, NavController, ActionSheet, NavParams} from 'ionic-angular';
import {NgForm} from 'angular2/common';

import {Ajax} from "../../../service/ajax";
import {UserService} from "../../../service/user";

import {SearchFilter} from "../../../filter/search";


@Page({
  templateUrl: 'build/pages/director/teacherList/teacherList.html',
  providers: [Ajax, UserService],
  pipes:[SearchFilter]
})
export class TeacherListPage{

	private approvedList :any = [];
	private notApprovedList :any = [];
	private keyword : string = "";
	private preschool: any = undefined;
	constructor(
		private ajax : Ajax,
		private user : UserService,
		private navParams : NavParams,
		private nav : NavController){
	
		this.preschool = this.navParams.get("preschool");
		console.log(this.preschool);
	}
	onPageWillEnter(){
		this.ajax.get({
			url:"/preschools/"+this.preschool.id+"/teachers"
		}).subscribe(
			data => {
				if(data.result == "success"){
					console.log(data);
				}
			},
			err => console.log(err)
		);
	}
	showApprovePopup(teacher){
		/*
		let actionSheet = ActionSheet.create({
			title:teacher.name +"("+student.class_name+")"	,
			buttons:[
				{
					text : "승인하기",
					icon : "checkmark",
					handler: () => {
					}
				},
				{
					text : "승인거부",
					icon : "hand",
					handler: () => {
					}
				},

			]
		});
		this.nav.present(actionSheet);
		*/
	}

}


