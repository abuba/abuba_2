﻿import {Page, NavController, ViewController, NavParams, IonicApp} from 'ionic-angular';
import {UserService} from "../../../service/user";
import {TeacherListPage} from "../teacherList/teacherList";


@Page({
  templateUrl: 'build/pages/director/home/main.html',
  providers: [UserService]
})
export class DirectorMainPage{
	private preschool : any = {};
	constructor(
		private user : UserService,
		private nav : NavController,
		private viewCtrl : ViewController,
		private navParams : NavParams,
		private app : IonicApp
	){


	}
	onPageWillEnter(){
		//this.viewCtrl.showBackButton(false);
		this.preschool = this.navParams.get("preschool") ;
		console.log(this.preschool);
	}
	onPageDidEnter(){
		//this.nav.setRoot(DirectorHomePage);
		//this.setRoot(DirectorHomePage);
		//this.nav.remove(0, this.nav.length()-1);
	}
	teacher(){
		let rootNav = this.app.getComponent("rootNavController");
		rootNav.push(TeacherListPage, {
			preschool : this.preschool
		});

	}
	driver(){

	}

}
