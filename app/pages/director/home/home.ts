﻿import {Page, NavController, ViewController, IonicApp} from 'ionic-angular';
import {UserService} from "../../../service/user";
import {Ajax} from "../../../service/ajax";
import {DirectorMainPage} from "./main";
import {PreschoolService } from "../../../service/director/preschool"

import {TestPage} from "../../test/test";
import {LoginPage} from "../../login/login";


@Page({
  templateUrl: 'build/pages/director/home/home.html',
  providers: [Ajax, UserService, PreschoolService]
})
export class DirectorHomePage{
	private preschoolList :any= [];
	private rootNav:NavController = undefined;
	constructor(
		private user : UserService,
		private nav : NavController,
		private viewCtrl : ViewController,
		private app : IonicApp,
		private preschoolService : PreschoolService
	){
		this.rootNav = this.app.getComponent("rootNavController");
		this.preschoolService.get().subscribe(
			list => {
				this.preschoolList = list;
				app.getComponent("directorNav").setRoot(DirectorMainPage, {
					preschool : this.preschoolList[0]
				});
			},
			err => {
				console.log(err);
			}
		);
			//this.rootPage = DirectorMainPage;

	}
	onPageWillEnter(){
		this.preschoolService.get().subscribe(
			list => {
				this.preschoolList = list;
			},
			err => {
				console.log(err);
			}
		);
	}
	onPageDidEnter(){
	}

	openPreschool(p){

	}
	logout(){
		window.localStorage.clear();
		this.rootNav.setRoot(LoginPage);
	}


}
