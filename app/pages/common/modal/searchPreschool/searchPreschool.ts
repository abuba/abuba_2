﻿import {Page, NavController, ViewController} from 'ionic-angular';
import {Ajax} from "../../../../service/ajax";


@Page({
templateUrl: 'build/pages/common/modal/searchPreschool/searchPreschool.html',
	providers:[Ajax]
})
export class SearchPreschoolModal {
	keyword : string = ""
	preschoolList : any = []
	constructor(
		private ajax : Ajax, 
		private viewCtrl : ViewController

	){ }

	close(){
		this.viewCtrl.dismiss();
	}

	search(){
		console.log(this.keyword);
		this.ajax.get({
			url:"/preschools?keyword="+this.keyword
		}).subscribe(
			data => {
				this.preschoolList = data.data;
			},
			err => console.log(err)
		);
	}
	selPreschool(preschool){
		this.viewCtrl.dismiss(preschool);

	}

}
