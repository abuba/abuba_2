﻿import {Page, NavController, ViewController, NavParams, ActionSheet} from 'ionic-angular';
import {UserService} from "../../../service/user";


@Page({
  templateUrl: 'build/pages/driver/home/main.html',
  providers: [UserService]
})
export class TeacherMainPage{
	private preschool : any = undefined;
	constructor(
		private user : UserService,
		private nav : NavController,
		private viewCtrl : ViewController,
		private navParams : NavParams
		){


	}
	onPageWillEnter(){
		
	}
	onPageDidEnter(){
	}

}
