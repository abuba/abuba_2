﻿import {Page, NavController, ViewController, IonicApp} from 'ionic-angular';
import {UserService} from "../../../service/user";
import {Ajax} from "../../../service/ajax";
import {DriverMainPage} from "./main";

import {StudentListPage} from "../studentList/studentList"

import {TestPage} from "../../test/test";


@Page({
  templateUrl: 'build/pages/driver/home/home.html',
  providers: [Ajax, UserService]
})
export class TeacherHomePage{
	private preschoolList :any= [];
	constructor(
		private user : UserService,
		private nav : NavController,
		private viewCtrl : ViewController,
		private app : IonicApp
	){
		this.rootPage = TeacherMainPage;

	}
	onPageWillEnter(){
	}
	onPageDidEnter(){
	}

}
