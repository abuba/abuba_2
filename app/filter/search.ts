import {Pipe} from "angular2/core"
@Pipe({
	name : "search"
})
export class SearchFilter {
	transform(list, arg){
		return list.filter((item) => item[arg[0]].indexOf(arg[1])  != -1)
	}
}
