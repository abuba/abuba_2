import {App, Platform, NavController} from 'ionic-angular';
import {ViewChild} from "angular2/core";
import {StatusBar, Keyboard} from 'ionic-native';
import {Storage} from "./service/storage";
import {UserService} from "./service/user";

import {LoginPage} from './pages/login/login';
import {SelectRolePage} from './pages/selectRole/selectRole';
import {ParentHomePage} from './pages/parent/home/parentHome';
import {DirectorHomePage} from './pages/director/home/home';
import {TeacherHomePage} from './pages/teacher/home/home';

@App({
	template: '<ion-nav #rootNavController id="rootNavController" [root]="rootPage"></ion-nav>',
	config: {}, // http://ionicframework.com/docs/v2/api/config/Config/
	providers:[UserService, Storage]
})
export class MyApp {
	rootPage: any = LoginPage;
	@ViewChild("rootNavController") nav:NavController

	constructor(platform: Platform, userService:UserService ) {
		let savedUser = userService.get();
		if(savedUser){
			if(savedUser.mem_type == 0){
				this.rootPage = SelectRolePage;
			}else if(savedUser.mem_type == 1){
				this.rootPage = ParentHomePage;
			}else if(savedUser.mem_type ==2){
				this.rootPage = TeacherHomePage;
			}else if(savedUser.mem_type ==3){
				this.rootPage = DirectorHomePage;
			}
		}else{
			this.rootPage = LoginPage;
		}
		platform.ready().then(() => {
			// Okay, so the platform is ready and our plugins are available.
			// Here you can do any higher level native things you might need.
			StatusBar.styleDefault();
			Keyboard.hideKeyboardAccessoryBar(true);
			Keyboard.disableScroll(true);
			
			document.addEventListener('backbutton', () => {
				let activeVC = this.nav.getActive();
				let page = activeVC.instance;

				if (!this.nav.canGoBack()) {
					console.log('Exiting app due to back button press at the root view');
					return navigator.app.exitApp();
				}
				console.log('Detected a back button press outside of tabs page - popping a view from the root navigation stack');
				return this.nav.pop();

				/*
				if (!(page instanceof TabsPage)) {
					if (!this.nav.canGoBack()) {
						console.log('Exiting app due to back button press at the root view');
						return navigator.app.exitApp();
					}
					console.log('Detected a back button press outside of tabs page - popping a view from the root navigation stack');
					return this.nav.pop();
				}

				let tabs = page.tabs;
				let activeNav = tabs.getSelected();

				if (!activeNav.canGoBack()) {
					console.log('Exiting app due to back button press at the bottom of current tab\'s navigation stack');
					return navigator.app.exitApp();
				}

				console.log('Detected a back button press - popping a view from the current tab\'s navigation stack');
				return activeNav.pop();
				*/

			}, false);
		});
	}
}
